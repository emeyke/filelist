﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EMeyke.FileList
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    memoEdit1.Text = "";
                    DirectoryInfo dir = new DirectoryInfo(fbd.SelectedPath);

                    DirectoryInfo[] dirs = dir.GetDirectories();
                    foreach (DirectoryInfo item in dirs)
                    {
                        if (memoEdit1.Text == "")
                            memoEdit1.Text = item.Name + "\t" + DirSize(item)+ "\r\n";
                        else
                            memoEdit1.Text = memoEdit1.Text + item.Name + "\t"+ DirSize(item) + "\r\n";
                    }

                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    foreach (String item in files)
                    {
                        FileInfo fi = new FileInfo(item);
                        if (memoEdit1.Text == "")
                            memoEdit1.Text = Path.GetFileNameWithoutExtension(item) + "\t" + fi.Length.ToString() + "\t" + item + "\r\n";
                        else
                            memoEdit1.Text = memoEdit1.Text + Path.GetFileNameWithoutExtension(item) + "\t" + fi.Length.ToString() + "\t" + item + "\r\n";
                    }
                }
            }


       

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
